# vihannes-be

Ruoka rähinä koodihaastetta varten tehty Spring Boot backend.

Api dokumentaatio Swagger: https://dry-harbor-71047.herokuapp.com/swagger-ui/ tai http://localhost:8080/swagger-ui/

Rajapinnassa on kaksi kutsua. Esimerkit lokaalia ja heroku ympäristöä vasten:
- http://localhost:8080/vihannesTiedot?id=300
- https://dry-harbor-71047.herokuapp.com/vihannesTiedot?id=300
- http://localhost:8080/rahina?f1=300&f2=303
- https://dry-harbor-71047.herokuapp.com/rahina?f1=300&f2=303

Heroku sovellus voi tarvita hetken aikaa reagointiin, koska sovellus sammuu jos sitä ei käytetä pitkään aikaan. Eli kärsivällisyyttä testatessa.
VihannesTiedot hakee kannasta tai finelin rajapinnasta ruuan tiedot. 
Jos ruokaa ei löytynyt kannasta tämä tallennetaan kantaan finelin rajapinnasta.
Rahinalla aloitetaan taistelu kahden ruuan välillä. Ruokien tulee löytyä ensin kannasta, jotta taistelu onnistuu.

Muutama testi: https://gitlab.com/nicotus/vihannes-be/-/tree/main/src/test/java/fi/vihannes/vihannesbe

## Teknologiat ja kehitysympäristö
### Teknologiat
- Spring-boot 2.7.4
- Java 17
- Junit 4
- H2
- Swagger

### Kehittäjän kehitysympäristö
- IntelliJ IDEA Community Edition 2020
- Windows 10

## Deployment ja testaus

- Lokaalisti käynnistäminen: mvn spring-boot:run
- Testit: mvn clean test
- Projekti on jaeltu Herokuun
  - Sinne deploy tapahtui kehittäjän koneelta heroku cli:llä
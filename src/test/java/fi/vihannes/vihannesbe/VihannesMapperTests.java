package fi.vihannes.vihannesbe;

import fi.vihannes.vihannesbe.domain.Vihannes;
import fi.vihannes.vihannesbe.domain.VihannesTaistelija;
import fi.vihannes.vihannesbe.entity.VihannesEntity;
import fi.vihannes.vihannesbe.mapper.VihannesMapper;
import org.junit.Assert;
import org.junit.Test;

public class VihannesMapperTests {
    @Test
    public void vihannesMapperToEntityTest() {
        Vihannes alkuperainenVihannes = new Vihannes(20, "2.0", "3.0", "4.0", "5.0");
        VihannesMapper mapper = new VihannesMapper();

        VihannesEntity uusiVihannesEntity = mapper.toEntity(alkuperainenVihannes);

        Assert.assertEquals(20, uusiVihannesEntity.getId());
        Assert.assertEquals("2.0", uusiVihannesEntity.getCarbohydrate());
        Assert.assertEquals("3.0", uusiVihannesEntity.getProtein());
        Assert.assertEquals("4.0", uusiVihannesEntity.getEnergyKcal());
        Assert.assertEquals("5.0", uusiVihannesEntity.getFat());
    }

    @Test
    public void vihannesMapperToVihannesTest() {
        VihannesEntity alkuperainenVihannesEntity = new VihannesEntity(425, "23.0", "13.0", "45.0", "25.0");
        VihannesMapper mapper = new VihannesMapper();

        Vihannes uusiVihannes = mapper.toVihannes(alkuperainenVihannesEntity);

        Assert.assertEquals(425, uusiVihannes.getId());
        Assert.assertEquals("23.0", uusiVihannes.getCarbohydrate());
        Assert.assertEquals("13.0", uusiVihannes.getProtein());
        Assert.assertEquals("45.0", uusiVihannes.getEnergyKcal());
        Assert.assertEquals("25.0", uusiVihannes.getFat());
    }

    @Test
    public void vihannesMapperTeeVihanneksestaTaistelijaTest() {
        VihannesEntity alkuperainenVihannesEntity = new VihannesEntity(425, "23.0", "13.0", "45.0", "25.0");
        VihannesMapper mapper = new VihannesMapper();

        VihannesTaistelija uusiVihannesTaistelija = mapper.teeVihanneksestaTaistelija(alkuperainenVihannesEntity);

        Assert.assertEquals(425, uusiVihannesTaistelija.getId());
        Assert.assertEquals(23.0f, uusiVihannesTaistelija.getHyokkaysVoima(), 0.1);
        Assert.assertEquals(13.0f, uusiVihannesTaistelija.getPuolustusVoima(), 0.1);
        Assert.assertEquals(45.0f, uusiVihannesTaistelija.getElama(), 0.1);
        Assert.assertEquals(61, uusiVihannesTaistelija.getHyokkausNopeus());
    }
}

package fi.vihannes.vihannesbe;

import fi.vihannes.vihannesbe.domain.Vihannes;
import fi.vihannes.vihannesbe.entity.VihannesEntity;
import fi.vihannes.vihannesbe.repository.VihannesRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes =  VihannesBeApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class VihannesRestControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private VihannesRepository vihannesRepo;

    @Test
    public void vihannesControllerGetVihannesTiedot_VihannesLoytyyKannastaJaStatus200Test() throws Exception{
        Mockito.when(vihannesRepo.findById(300)).thenReturn(new VihannesEntity(300, "2.0", "3.0", "4.0", "5.0"));

        mvc.perform(get("/vihannesTiedot?id=300"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void vihannesControllerGetVihannesTiedot_VihannesEiLoydyKannastaMuttaLoytyyRajapinnastaJaStatus200Test() throws Exception{
        Vihannes mockVihannes = new Vihannes(666, "2.0", "3.0", "4.0", "5.0");
        Mockito.when(restTemplate.getForObject("https://fineli.fi/fineli/api/v1/foods/666", Vihannes.class))
                .thenReturn(mockVihannes);

        mvc.perform(get("/vihannesTiedot?id=666"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void vihannesControllerGetRahina_VihanneksetTaistelevatJaStatus200Test() throws Exception{
        Mockito.when(vihannesRepo.findById(300)).thenReturn(new VihannesEntity(300, "8.0", "1.0", "54.0", "1.0"));
        Mockito.when(vihannesRepo.findById(303)).thenReturn(new VihannesEntity(303, "5.0", "1.5", "30.5", "2.0"));

        mvc.perform(get("/rahina?f1=300&f2=303"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
}

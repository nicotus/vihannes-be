package fi.vihannes.vihannesbe;

import fi.vihannes.vihannesbe.component.VihannesComponent;
import fi.vihannes.vihannesbe.domain.TaisteluVuoro;
import fi.vihannes.vihannesbe.domain.Vihannes;
import fi.vihannes.vihannesbe.domain.VihannesTaistelija;
import fi.vihannes.vihannesbe.entity.VihannesEntity;
import fi.vihannes.vihannesbe.repository.VihannesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class VihannesComponentTests {

	private static final Logger log = LoggerFactory.getLogger(VihannesComponentTests.class);

	@Mock
	private VihannesRepository mockVihannesRepo;

	@Mock
	private	RestTemplate restTemplate;

	@InjectMocks
	private VihannesComponent vihannesComponent;

	@Test
	public void vihannesComponentEtsiVihannesTest() throws Exception {
		Mockito.when(mockVihannesRepo.findById(20)).thenReturn(new VihannesEntity(20, "2.0", "3.0", "4.0", "5.0"));
		Vihannes mockedVihannes = vihannesComponent.etsiVihannes(20);

		Assert.assertEquals(20, mockedVihannes.getId());
		Assert.assertEquals("2.0", mockedVihannes.getCarbohydrate());
		Assert.assertEquals("3.0", mockedVihannes.getProtein());
		Assert.assertEquals("4.0", mockedVihannes.getEnergyKcal());
		Assert.assertEquals("5.0", mockedVihannes.getFat());
	}

	@Test
	public void vihannesRepositoryFindAllTest() throws Exception {
		List<VihannesEntity> testEntityList = new ArrayList<>();
		testEntityList.add(new VihannesEntity(20, "2.0", "3.0", "4.0", "5.0"));
		testEntityList.add(new VihannesEntity(31, "3.1", "4.1", "5.1", "6.1"));
		Mockito.when(mockVihannesRepo.findAll()).thenReturn(testEntityList);

		List<VihannesEntity> mockedVihannesEntityList = mockVihannesRepo.findAll();

		Assert.assertEquals(2, mockedVihannesEntityList.size());
		Assert.assertEquals(testEntityList, mockedVihannesEntityList);
	}

	@Test
	public void vihannesComponentValmisteleTaistelijaTest() {
		Mockito.when(mockVihannesRepo.findById(20)).thenReturn(new VihannesEntity(20, "2.0", "3.0", "4.0", "5.0"));
		VihannesTaistelija mockedVihannesTaistelija = vihannesComponent.valmisteleTaistelija(20);

		Assert.assertEquals(20, mockedVihannesTaistelija.getId());
		Assert.assertEquals(2.0f, mockedVihannesTaistelija.getHyokkaysVoima(), 0.1);
		Assert.assertEquals(3.0f, mockedVihannesTaistelija.getPuolustusVoima(), 0.1);
		Assert.assertEquals(4.0f, mockedVihannesTaistelija.getElama(), 0.1);
		Assert.assertEquals(10L, mockedVihannesTaistelija.getHyokkausNopeus());
	}

	@Test
	public void vihannesComponentHyokkaaToisenKimppuunVaurioitaTest() {
		VihannesTaistelija vt1 = new VihannesTaistelija(11, 20.6f, 10.6f, 2.5f, 30);
		VihannesTaistelija vt2 = new VihannesTaistelija(24, 45.1f, 5.1f, 4.5f, 20);

		vihannesComponent.hyokkaaToisenKimppuun(vt1, vt2);

		Assert.assertEquals("Vastustaja vt2 on saanut vauriota 5.1 arvon edestä",34.9f, vt2.getElama(), 0.1);
	}

	@Test
	public void vihannesComponentHyokkaaToisenKimppuunEiVauriotaTest() {
		VihannesTaistelija vt1 = new VihannesTaistelija(11, -3.4f, 10.6f, 2.5f, 30);
		VihannesTaistelija vt2 = new VihannesTaistelija(24, 45.1f, 5.1f, 4.5f, 20);

		vihannesComponent.hyokkaaToisenKimppuun(vt1, vt2);

		Assert.assertEquals("Vastustaja vt2 ei ole saanut vauriota",45.0f, vt2.getElama(), 0.1);
	}

	@Test
	public void vihannesComponentAjastimienLuontiTest() throws Exception {
		VihannesTaistelija vt1 = new VihannesTaistelija(11, 20.6f, 10.6f, 2.5f, 5);
		VihannesTaistelija vt2 = new VihannesTaistelija(24, 45.1f, 5.1f, 4.5f, 3);
		List<TaisteluVuoro> taisteluVuoroList = new ArrayList<>();
		long aloitusAika = System.currentTimeMillis();

		vihannesComponent.luoTaisteluAjastimet(aloitusAika, vt1, vt2, taisteluVuoroList);

		// Odotetaan 0.5 sekunttia, että ajastimet tekevät tehtävänsä
		Thread.sleep(500);

		Assert.assertTrue("Taistelija vt1 on saanut vauriota", vt1.getElama() < 20.6f);
		Assert.assertTrue("Taistelija vt2 on saanut vauriota", vt2.getElama() < 45.1f);
	}

	@Test
	public void vihannesComponentTarkistaOvatkoTaistelijatHengissaKuollellaVihanneksellaTest() {
		VihannesTaistelija vt1 = new VihannesTaistelija(11, 20.6f, 10.6f, 2.5f, 5);
		VihannesTaistelija vt2 = new VihannesTaistelija(24, -3.1f, 5.1f, 4.5f, 3);

		Assert.assertFalse("Toinen taistelijoista ei ole hengissä, joten pitäisi palautua false",
				vihannesComponent.tarkistaOvatkoTaistelijatHengissa(vt1, vt2));
	}

	@Test
	public void vihannesComponentTarkistaOvatkoTaistelijatHengissaElavillaVihanneksillaTest() {
		VihannesTaistelija vt1 = new VihannesTaistelija(11, 5.6f, 10.6f, 2.5f, 5);
		VihannesTaistelija vt2 = new VihannesTaistelija(24, 1.1f, 5.1f, 4.5f, 3);

		Assert.assertTrue("Molemmat taistelijoista ovat hengissä, joten pitäisi palautua true",
				vihannesComponent.tarkistaOvatkoTaistelijatHengissa(vt1, vt2));
	}

	@Test
	public void vihannesComponentLisaaVihannesKantaanTest() {
		Vihannes mockVihannes = new Vihannes(35, "2.0", "3.0", "4.0", "5.0");
		Mockito.when(restTemplate.getForObject("https://fineli.fi/fineli/api/v1/foods/35", Vihannes.class))
				.thenReturn(mockVihannes);

		Mockito.when(mockVihannesRepo.save(Mockito.any(VihannesEntity.class))).thenAnswer(i -> i.getArguments()[0]);

		Vihannes mockedVihannes = vihannesComponent.addVihannesToDatabase(35);

		Assert.assertEquals(35, mockedVihannes.getId());
		Assert.assertEquals("2.0", mockedVihannes.getCarbohydrate());
		Assert.assertEquals("3.0", mockedVihannes.getProtein());
		Assert.assertEquals("4.0", mockedVihannes.getEnergyKcal());
		Assert.assertEquals("5.0", mockedVihannes.getFat());
	}
}

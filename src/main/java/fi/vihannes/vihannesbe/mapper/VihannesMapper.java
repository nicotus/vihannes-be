package fi.vihannes.vihannesbe.mapper;

import fi.vihannes.vihannesbe.domain.Vihannes;
import fi.vihannes.vihannesbe.domain.VihannesTaistelija;
import fi.vihannes.vihannesbe.entity.VihannesEntity;
import org.springframework.stereotype.Component;

@Component
public class VihannesMapper {
    public VihannesEntity toEntity(Vihannes vihannes) {
        int id = vihannes.getId();
        String carbohydrate = vihannes.getCarbohydrate();
        String protein = vihannes.getProtein();
        String energyKcal = vihannes.getEnergyKcal();
        String fat = vihannes.getFat();

        return new VihannesEntity(id, carbohydrate, protein, energyKcal, fat);
    }

    public Vihannes toVihannes(VihannesEntity vihannesEntity) {
        int id = vihannesEntity.getId();
        String carbohydrate = vihannesEntity.getCarbohydrate();
        String protein = vihannesEntity.getProtein();
        String energy = vihannesEntity.getEnergyKcal();
        String fat = vihannesEntity.getFat();

        return new Vihannes(id, carbohydrate, protein, energy, fat);
    }

    public VihannesTaistelija teeVihanneksestaTaistelija(VihannesEntity vihannesEntity) {
        int id = vihannesEntity.getId();
        float carbohydrate = Float.parseFloat(vihannesEntity.getCarbohydrate());
        float protein = Float.parseFloat(vihannesEntity.getProtein());
        float energyKcal = Float.parseFloat(vihannesEntity.getEnergyKcal());
        float fat = Float.parseFloat(vihannesEntity.getFat());
        float taisteluNopeus = fat + protein + carbohydrate;

        return new VihannesTaistelija(id, energyKcal, carbohydrate, protein, Math.round(taisteluNopeus));
    }
}

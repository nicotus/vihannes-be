package fi.vihannes.vihannesbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VihannesBeApplication {
	public static void main(String[] args) {
		SpringApplication.run(VihannesBeApplication.class, args);
	}
}

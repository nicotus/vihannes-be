package fi.vihannes.vihannesbe.restservice;

import fi.vihannes.vihannesbe.VihannesBeApplication;
import fi.vihannes.vihannesbe.component.VihannesComponent;
import fi.vihannes.vihannesbe.domain.TaisteluVuoro;
import fi.vihannes.vihannesbe.domain.Vihannes;
import fi.vihannes.vihannesbe.domain.VihannesTaistelija;
import fi.vihannes.vihannesbe.domain.VihannesTaisteluData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class VihannesTiedotController {

    private static final Logger log = LoggerFactory.getLogger(VihannesBeApplication.class);

    @Autowired
    private VihannesComponent vihannesComponent;

    @CrossOrigin(origins = "https://ruokarahina.herokuapp.com/")
    @GetMapping("/vihannesTiedot")
    public Vihannes vihannesTiedot(@RequestParam(value = "id") int id) {
        log.info("Hae tiety vihannes kannasta tai rajapinnasta");

        return vihannesComponent.etsiVihannes(id);
    }

    @CrossOrigin(origins = "https://ruokarahina.herokuapp.com/")
    @GetMapping("/rahina")
    public VihannesTaisteluData vihannesTaisteluData(@RequestParam(value = "f1") int fighterId1,
                                                     @RequestParam(value = "f2") int fighterId2) {
        log.info("Taistelu alkakoon! Taistelija 1 id = " + fighterId1 + " vastaan taistelija 2 id = " + fighterId2);

        VihannesTaistelija vihannesTaistelija1 = vihannesComponent.valmisteleTaistelija(fighterId1);
        VihannesTaistelija vihannesTaistelija2 = vihannesComponent.valmisteleTaistelija(fighterId2);

        log.info(vihannesTaistelija1.toString());
        log.info(vihannesTaistelija2.toString());

        List<TaisteluVuoro> taisteluVuoroList = new ArrayList<>();
        long timeout = 10000;
        long aloitusAika = System.currentTimeMillis();

        vihannesComponent.luoTaisteluAjastimet(aloitusAika, vihannesTaistelija1, vihannesTaistelija2, taisteluVuoroList);

        // Jos tulee tasapeli voittajaId on -1
        int voittajaId;
        while (true) {
            if (!vihannesComponent.tarkistaOvatkoTaistelijatHengissa(vihannesTaistelija1, vihannesTaistelija2)) {
                if (vihannesTaistelija1.getElama() >= 0.0f) voittajaId = vihannesTaistelija1.getId();
                else {
                    voittajaId = vihannesTaistelija2.getId();
                }
                log.info("Toinen taistelijoista tuhottiin! Voitaaja on = " + voittajaId);

                break;
            }

            if ((System.currentTimeMillis() - aloitusAika) >= timeout) {
                log.info("Tasapeli!");
                voittajaId = -1;

                break;
            }
        }
        return new VihannesTaisteluData(taisteluVuoroList, voittajaId);
    }
}

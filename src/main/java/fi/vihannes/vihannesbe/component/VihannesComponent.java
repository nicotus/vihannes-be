package fi.vihannes.vihannesbe.component;

import fi.vihannes.vihannesbe.domain.TaisteluVuoro;
import fi.vihannes.vihannesbe.domain.Vihannes;
import fi.vihannes.vihannesbe.domain.VihannesTaistelija;
import fi.vihannes.vihannesbe.mapper.VihannesMapper;
import fi.vihannes.vihannesbe.repository.VihannesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Component
public class VihannesComponent {

    private static final VihannesMapper vihannesMapper = new VihannesMapper();

    private static final Logger log = LoggerFactory.getLogger(VihannesComponent.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private VihannesRepository vihannesRepository;

    ReadWriteLock lock = new ReentrantReadWriteLock();
    Lock writeLock = lock.writeLock();
    Lock readLock = lock.readLock();

    public VihannesComponent() {
    }

    public Vihannes etsiVihannes(int id) {
        if (vihannesRepository.findById(id) != null) {
            return vihannesMapper.toVihannes(vihannesRepository.findById(id));
        } else {
            return addVihannesToDatabase(id);
        }
    }

    private void addAllConvertersToRestTemplate() {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);
    }

    public Vihannes addVihannesToDatabase(int vihannesID) {
        addAllConvertersToRestTemplate();

        Vihannes vihannes = restTemplate.getForObject("https://fineli.fi/fineli/api/v1/foods/" + vihannesID, Vihannes.class);

        vihannesRepository.save(vihannesMapper.toEntity(vihannes));

        return vihannes;
    }

    public VihannesTaistelija valmisteleTaistelija(int taistelijaId) {
        return vihannesMapper.teeVihanneksestaTaistelija(vihannesRepository.findById(taistelijaId));
    }

    public boolean hyokkaaToisenKimppuun (VihannesTaistelija hyokkaaja, VihannesTaistelija puolustaja) {
        if (tarkistaOvatkoTaistelijatHengissa(hyokkaaja, puolustaja)) {
            try {
                writeLock.lock();
                puolustaja.setElama(puolustaja.getElama() - laskeHyokkauksenVaurio(hyokkaaja.getHyokkaysVoima(), puolustaja.getPuolustusVoima()));
            } finally {
                writeLock.unlock();
            }
            return false;
        } else return true;
    }

    private float laskeHyokkauksenVaurio(float hyokkausVoima, float puolustusVoima) {
        return hyokkausVoima * (100.0f - puolustusVoima) / 100.0f;
    }

    public boolean tarkistaOvatkoTaistelijatHengissa(VihannesTaistelija t1, VihannesTaistelija t2) {
        try {
            readLock.lock();
            if (t1.getElama() <= 0.0f  || t2.getElama() <= 0.0f) return false;
        } finally {
            readLock.unlock();
        }
        return true;
    }

    public void luoTaisteluAjastimet(long aloitusAika, VihannesTaistelija vt1, VihannesTaistelija vt2, List<TaisteluVuoro> taisteluVuoroList) {
        TimerTask taistelija1TaskTimer = new TimerTask() {
            @Override
            public void run() {
                // Hyökkää
                log.info("Hyökkää Vihannes 1! ajassa: " + (System.currentTimeMillis() - aloitusAika));
                if (hyokkaaToisenKimppuun(vt1, vt2)) this.cancel();
                else {
                    log.info("Lisätään lyönti vuorolistaan");
                    // Tallenna hyökkäys lopputulos
                    taisteluVuoroList.add(new TaisteluVuoro(vt1.getId(),
                            (System.currentTimeMillis() - aloitusAika), vt1.getHyokkaysVoima(), vt2.getElama()));
                }

            }
        };

        TimerTask taistelija2TaskTimer = new TimerTask() {
            @Override
            public void run() {
                // Hyökkää
                log.info("Hyökkää Vihannes 2! ajassa: " + (System.currentTimeMillis() - aloitusAika));
                if (hyokkaaToisenKimppuun(vt2, vt1)) {
                    log.info("Hyökkäys epäonnistui!");
                    this.cancel();
                }
                else {
                    // Tallenna hyökkäys lopputulos
                    taisteluVuoroList.add(new TaisteluVuoro(vt2.getId(),
                            (System.currentTimeMillis() - aloitusAika), vt2.getHyokkaysVoima(), vt1.getElama()));
                }

            }
        };

        Timer timer = new Timer("Timer1") ;
        timer.scheduleAtFixedRate(taistelija1TaskTimer, vt1.getHyokkausNopeus() * 10, vt1.getHyokkausNopeus() * 10);
        timer.scheduleAtFixedRate(taistelija2TaskTimer, vt2.getHyokkausNopeus() * 10, vt2.getHyokkausNopeus() * 10);
    }

}

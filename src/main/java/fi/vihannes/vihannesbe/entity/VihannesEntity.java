package fi.vihannes.vihannesbe.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class VihannesEntity {

    @Id
    private int id;
    private String carbohydrate;
    private String protein;
    private String energyKcal;
    private String fat;

    public VihannesEntity() {
    }

    public VihannesEntity(int id, String carbohydrate, String protein, String energyKcal, String fat) {
        this.id = id;
        this.carbohydrate = carbohydrate;
        this.protein = protein;
        this.energyKcal = energyKcal;
        this.fat = fat;
    }

    public int getId() {
        return id;
    }

    public String getCarbohydrate() {
        return carbohydrate;
    }

    public String getProtein() {
        return protein;
    }

    public String getEnergyKcal() {
        return energyKcal;
    }

    public String getFat() {
        return fat;
    }

    @Override
    public String toString() {
        return "VihannesEnitity{" +
                "id=" + id +
                ", carbohydrate='" + carbohydrate + '\'' +
                ", protein='" + protein + '\'' +
                ", energy='" + energyKcal + '\'' +
                ", fat='" + fat + '\'' +
                '}';
    }
}

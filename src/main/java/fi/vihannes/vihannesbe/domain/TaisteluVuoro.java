package fi.vihannes.vihannesbe.domain;

public class TaisteluVuoro {
    private int hyokkaajaId;
    private float aika;
    private float vaurio;
    private float vastustajanElama;

    public TaisteluVuoro() {
    }

    public TaisteluVuoro(int hyokkaajaId, float aika, float vaurio, float vastustajanElama) {
        this.hyokkaajaId = hyokkaajaId;
        this.aika = aika;
        this.vaurio = vaurio;
        this.vastustajanElama = vastustajanElama;
    }

    public int getHyokkaajaId() {
        return hyokkaajaId;
    }

    public void setHyokkaajaId(int hyokkaajaId) {
        this.hyokkaajaId = hyokkaajaId;
    }

    public float getAika() {
        return aika;
    }

    public void setAika(float aika) {
        this.aika = aika;
    }

    public float getVaurio() {
        return vaurio;
    }

    public void setVaurio(int vaurio) {
        this.vaurio = vaurio;
    }

    public float getVastustajanElama() {
        return vastustajanElama;
    }

    public void setVastustajanElama(int vastustajanElama) {
        this.vastustajanElama = vastustajanElama;
    }

    @Override
    public String toString() {
        return "TaisteluVuoro{" +
                "hyokkaajaId=" + hyokkaajaId +
                ", aika=" + aika +
                ", vaurio=" + vaurio +
                ", vastustajanElama=" + vastustajanElama +
                '}';
    }
}

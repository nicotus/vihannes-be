package fi.vihannes.vihannesbe.domain;

import java.util.List;

public class VihannesTiedot {
    private final List<Vihannes> vihannesLista;

    public VihannesTiedot(List<Vihannes> vihannesLista) {
        this.vihannesLista = vihannesLista;
    }

    public List<Vihannes> getVihannesLista() {
        return vihannesLista;
    }
}

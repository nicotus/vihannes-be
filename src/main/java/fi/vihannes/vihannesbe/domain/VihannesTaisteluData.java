package fi.vihannes.vihannesbe.domain;

import java.util.List;

public class VihannesTaisteluData {
    private final List<TaisteluVuoro> taisteluVuoroList;

    private final int voittajaId;


    public VihannesTaisteluData(List<TaisteluVuoro> taisteluVuoroList, int voittajaId) {
        this.voittajaId = voittajaId;
        this.taisteluVuoroList = taisteluVuoroList;
    }

    public int getVoittajaId() {
        return voittajaId;
    }

    public List<TaisteluVuoro> getTaisteluVuoroList() {
        return taisteluVuoroList;
    }
}

package fi.vihannes.vihannesbe.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vihannes {

    private int id;
    private String carbohydrate;
    private String protein;
    private String energyKcal;
    private String fat;

    public Vihannes() {
    }

    public Vihannes(int id, String carbohydrate, String protein, String energyKcal, String fat) {
        this.id = id;
        this.carbohydrate = carbohydrate;
        this.protein = protein;
        this.energyKcal = energyKcal;
        this.fat = fat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(String carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public String getEnergyKcal() {
        return energyKcal;
    }

    public void setEnergyKcal(String energyKcal) {
        this.energyKcal = energyKcal;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    @Override
    public String toString() {
        return "Vihannes{" +
                "id=" + id +
                ", carbohydrate='" + carbohydrate + '\'' +
                ", protein='" + protein + '\'' +
                ", energyKcal='" + energyKcal + '\'' +
                ", fat='" + fat + '\'' +
                '}';
    }
}

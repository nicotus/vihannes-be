package fi.vihannes.vihannesbe.domain;

public class VihannesTaistelija {
    private int id;
    private float elama;
    private float hyokkaysVoima;
    private float puolustusVoima;
    private long hyokkausNopeus;

    public VihannesTaistelija() {
    }

    public VihannesTaistelija(int id, float elama, float hyokkaysVoima, float puolustusVoima, long hyokkausNopeus) {
        this.id = id;
        this.elama = elama;
        this.hyokkaysVoima = hyokkaysVoima;
        this.puolustusVoima = puolustusVoima;
        this.hyokkausNopeus = hyokkausNopeus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getElama() {
        return elama;
    }

    public void setElama(float elama) {
        this.elama = elama;
    }

    public float getHyokkaysVoima() {
        return hyokkaysVoima;
    }

    public void setHyokkaysVoima(float hyokkaysVoima) {
        this.hyokkaysVoima = hyokkaysVoima;
    }

    public float getPuolustusVoima() {
        return puolustusVoima;
    }

    public void setPuolustusVoima(float puolustusVoima) {
        this.puolustusVoima = puolustusVoima;
    }

    public long getHyokkausNopeus() {
        return hyokkausNopeus;
    }

    public void setHyokkausNopeus(long hyokkausNopeus) {
        this.hyokkausNopeus = hyokkausNopeus;
    }

    @Override
    public String toString() {
        return "VihannesTaistelija{" +
                "id = " + id +
                ", elama=" + elama +
                ", hyokkaysVoima=" + hyokkaysVoima +
                ", puolustusVoima=" + puolustusVoima +
                ", hyokkausNopeus=" + hyokkausNopeus +
                '}';
    }
}

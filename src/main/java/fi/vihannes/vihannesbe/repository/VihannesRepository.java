package fi.vihannes.vihannesbe.repository;

import fi.vihannes.vihannesbe.entity.VihannesEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VihannesRepository extends CrudRepository<VihannesEntity, Integer> {
    VihannesEntity findById(int id);
    List<VihannesEntity> findAll();
}
